"""
Benitez-Llambay, P. & Krapp L. 2019

Solution of the linear problem for the multispecies streaming instability. 
This is an example that shows how the solver.py script can be used without parallel computing.

"""

from pylab import *
import solver as gs

#Parameters    
h0  =  0.05
ok0 =  1.0
r0  =  1.0
vk0 =  ok0*r0
cs  =  h0*vk0
xi  =  2*cs**2/vk0**2*vk0
q   =  1.5

# Lin3 mode from http://adsabs.harvard.edu/abs/2019ApJS..241...25B
Kx = Kz = 50.0
eps = array([1.0,0.5])
ts  = array([0.0425,0.1])

# Steady-state background solutions 
vgx, vgy, vjx, vjy = gs.equilibrium_solution(eps,ts,q,xi,ok0)

#Normalization factor for the velocities
norm_factor = h0**2*vk0 
vgx /= norm_factor
vgy /= norm_factor
vjx /= norm_factor
vjy /= norm_factor

# Compute the eigenvalues
w,v = gs.compute_eigenvalues(eps,ts,Kx,Kz,q,h0,vgx,vgy,vjx,vjy,vec=True)

print "Maximum Growth rate = ", -(w.real).min()
