"""
Benitez-Llambay, P. & Krapp L. 2019

Solution of the linear problem for the multispecies streaming
instability.  This script is designed to run in one specific node with
a given number of processors. It uses multiprocessing for parallel
computing.  This script should not be modified. The relevant
parameters for the user must be modified in parallel_solver.py.

"""

from pylab import *
import multiprocessing
import time
import solver as gs
import json

####### Loop for parallel process
def build_map(procnum,eps,ts,Kkx,Kkz,q,h0,vgx,vgy,vjx,vjy,vec,return_map,return_freq):

    size_x = len(Kkx)
    size_z = len(Kkz)
    MAP    = zeros([size_z,size_x])
    OSC    = zeros([size_z,size_x])

    for j,Kz in enumerate(Kkz):
        for i,Kx in enumerate(Kkx):
            w,v = gs.compute_eigenvalues(eps,ts,Kx,Kz,q,h0,vgx,vgy,vjx,vjy,vec)
            index, = where( w.real == (w.real).min() )[0]
            MAP[j,i] = (w[index].real)
            OSC[j,i] = (w[index].imag)
    MAP[MAP>0.0] = 0.0
   
    return_map [procnum*size_z*size_x:(procnum+1)*size_z*size_x] = (MAP).ravel()
    return_freq[procnum*size_z*size_x:(procnum+1)*size_z*size_x] = (OSC).ravel()
# -----------------------------------------------------------

if __name__=='__main__':

    # Return parameters as dictionary -------------------------------------------------------------------------
    par = json.loads((sys.argv[1]))
    
    # Disk parameters -----------------------------------------------------------------------------------------
    h0  =  par['h0' ]
    ok0 =  par['ok0']
    r0  =  par['r0' ]
    vk0 =  par['vk0']
    cs  =  par['cs' ]
    xi  =  par['xi' ]
    q   =  par['q'  ]
    eta =  par['eta']

    ### -------------------------------------------------------------------------------------------------------
    # Particle-size distribution parameters -------------------------------------------------------------------

    # Stokes numbers: Ts_min and Ts_max
    ts_min = par['tsmin']
    ts_max = par['tsmax']

    # Slope distribution
    sq = par['slope']

    # Number of dust species
    nbin = par['Nspecies']

    # Total dust-to-gas density ratio
    eps_tot = par['Eps']

    ### -------------------------------------------------------------------------------------------------------
    # Setup parameters ----------------------------------------------------------------------------------------
    kxmin = par['KXmin']
    kxmax = par['KXmax']
    nx    = par['NX']
    kzmin = par['KZmin'] 
    kzmax = par['KZmax']                
    nz    = par['NZ'] 

    ### -------------------------------------------------------------------------------------------------------
    # Nodes information por parallel computing ----------------------------------------------------------------
    # Node number
    node = par['node']
    
    # task_per_node (usually the number of processors available in the node)
    task_per_node = par['tasks']

    # Init time for multiprocessing
    t0 = time.time()
    
    ### -------------------------------------------------------------------------------------------------------
    # Compute particle-size distribution ----------------------------------------------------------------------

    ts_input = logspace(log10(ts_min),log10(ts_max),nbin+1) 
    eps_bins = []
    ts_bins  = []
    for l in range(nbin):
        slope = 4-sq
        if (slope != 0 ):
            eps_  = eps_tot* (ts_input[l+1]**slope - ts_input[l]**slope)/( ts_input.max()**slope - ts_input.min()**slope )
        else:
            eps_  = eps_tot* log(ts_input[l+1]/ts_input[l])/log(ts_input.max()/ts_input.min())
            
        eps_bins.append(eps_)
        ts_bins.append( ts_input[l+1] )

    ## Input values for each fluid
    eps = array(eps_bins)
    ts  = array(ts_bins)

    if (nbin == 0):
        eps = array([eps_tot,])
        ts  = array([ts_max ,])
   
    ### -------------------------------------------------------------------------------------------------------
    # SOLVER --------------------------------------------------------------------------------------------------

    # Equilibrium solution
    vgx, vgy, vjx, vjy = gs.equilibrium_solution(eps,ts,q,xi,ok0)

    # Normalized velocities
    norm_factor = h0**2*vk0
    vgx /= norm_factor
    vgy /= norm_factor
    vjx /= norm_factor
    vjy /= norm_factor

    # Map domain 
    Kkx = logspace(log10(kxmin),log10(kxmax),nx)
    Kkz = logspace(log10(kzmin),log10(kzmax),nz)

    ## Output arrays for multiplrocessing
    MAP = multiprocessing.Array('d',range(nz*nx))
    OSC = multiprocessing.Array('d',range(nz*nx))

    ### Domain descomposition
    division_kz = nz/task_per_node
    division_kx = None 
    processes = []
    for iz in range(task_per_node):
        kz_node = Kkz[iz*division_kz:(iz+1)*division_kz]
        kx_node = Kkx
            
        vec     = False
        args_   = (iz,eps,ts,kx_node,kz_node,q,h0,vgx,vgy,vjx,vjy,vec,MAP,OSC)
        proc    = multiprocessing.Process(target=build_map, args=args_)

        processes.append(proc)
        proc.start()
        #print 'Starting process for', proc.name
        
    for i,process in enumerate(processes):
        process.join()
        #print '%s.exitcode = %s' % (process.name, process.exitcode)

    MAP_save  = (array(MAP[:]).reshape(nz,nx))
    OSC_save  = (array(OSC[:]).reshape(nz,nx))
    
    #print time.time() - t0, "seconds wall time"

    ### END SOLVER --------------------------------------------------------------------------------------------
    ### -------------------------------------------------------------------------------------------------------
        
    # Save data -----------------------------------------------------------------------------------------------
    ### It saves one output per node. MAP correspond to the growth rate and OSC to the oscillatory frequency
    ### of the most unstable mode for each Kx, Kz.
    directory = par['Output']+"/"
    print " "
    print "Saving data ", directory+"MAP_{0:d}".format(node)
    save(directory+"MAP_{0:d}".format(node),MAP_save)
    save(directory+"OSC_{0:d}".format(node),OSC_save)
    save(directory+"kx_{0:d}".format(node),Kkx)
    save(directory+"kz_{0:d}".format(node),Kkz)
