"""
Benitez-Llambay, P. & Krapp L. 2019

Solution of the linear problem for the multispecies streaming
instability. This script allows the user to set up the input
parameters.  This script is designed for running in one node.

To run the script use: 

   $ python parallel_solver.py <total number of nodes> <specific node>

For example, if 3 nodes are available, send a job to each node as: 
   
   $ python parallel_solver.py 3 0 
   $ python parallel_solver.py 3 1 
   $ python parallel_solver.py 3 2

The script splits the KX-domain in the total number of nodes and return
a map for each node. To reduce the computational time set the value of
tasks_per_node to the number of processor availables in each
node. Thus, a domain descomposition for KZ is done using
multiprocessing.

Each map of the outout correspond to a particular domain of the full
solution.  The outputs named MAP_n.npy correspond to the growth rate
(the maximum growth rate for each KX,KZ).  The outputs named OSC_n.npy
correspond to the oscillatory frequency associated with the growth
rate.  To visualize the maps the script buil_map.py can be used:

  $ python build_map.py <total number of nodes> <Output directory>

"""

import sys
import os
from pylab import *
import json

if len(sys.argv) == 1:
    print " "
    print "Run the script as:"
    print "python parallel_solver.py <total number of nodes> <specific node (integer from 0 to number of nodes -1)>"
    print " ===================================================================================================== "
    print " "
    exit()
    
# Directory for data
Output = "maps"    

# Disk parameters -----------------------------------------------------------------------------------------
h0  =  0.05
ok0 =  1.0
r0  =  1.0
vk0 =  ok0*r0
cs  =  h0*vk0
xi  =  2*cs**2/vk0**2*vk0
q   =  1.5
eta = cs**2/vk0**2

### -------------------------------------------------------------------------------------------------------
# Particle-size distribution parameters -------------------------------------------------------------------
# For nbin=1 set ts_max and eps_tot (ts_min and sq are ignored). ------------------------------------------
ts_min  = 1e-4  #Stokes number (MIN)
ts_max  = 1e-1  #Stokes number (MAX)
sq      = 3.5   # Slope distribution
nbin    = 16     # Number of dust species
eps_tot = 1.0   # Total dust-to-gas density ratio

### -------------------------------------------------------------------------------------------------------
# Setup parameters ----------------------------------------------------------------------------------------
kxmin = 1e-1
kxmax = 1e+3
kzmin = 1e-1 
kzmax = 1e+3
# Domain size
NX    = 60
NZ    = 60 

### -------------------------------------------------------------------------------------------------------
# Nodes information por parallel computing ----------------------------------------------------------------
# Node number
number_nodes = int(sys.argv[1]) #Total number of nodes = number of maps to gather (use build_map.py)
node         = int(sys.argv[2]) #Selec specific node

# task_per_node (usually the number of processors available in the selected node)
tasks_per_node = 10

### -------------------------------------------------------------------------------------------------------

print "Solution of the linear eigenvalue problem for the multispecies streaming instability"
print " ===================================== "
print "Parameters: "
print "Slope               = ", q
print "Stokes number (MAX) = ", ts_max
print "Stokes number (MIN) = ", ts_min
print "Dust species        = ", nbin
print "Epsilon             = ", eps_tot
print "KX (min)            = ", kxmin
print "KX (max)            = ", kxmax
print "KZ (min)            = ", kzmin
print "KZ (max)            = ", kzmax


### -------------------------------------------------------------------------------------------------------
# List of input paramenter for solver
### -------------------------------------------------------------------------------------------------------

# Domain descomposition for KX ----------------------------------------------------------------------------
Kx          = logspace(log10(kxmin),log10(kxmax),NX)
nx_local    = NX/number_nodes
kxmin_local = Kx[nx_local*node]
kxmax_local = Kx[nx_local*(node+1)-1]

# Imput parameters dictionary -----------------------------------------------------------------------------
input_para = {
    "h0"    : h0         , "ok0"  : ok0        , "r0"    : r0, "vk0"     : vk0       , "cs"  : cs      ,"xi": xi,"q": q,"eta": eta,
    "tsmin" : ts_min     , "tsmax": ts_max     , "slope" : sq, "Nspecies": nbin      , "Eps" : eps_tot ,
    "KXmin" : kxmin_local, "KXmax": kxmax_local, "KZmin" : kzmin , "KZmax": kzmax    , "NZ"  : NZ      , "NX": nx_local,
    "node"  : node       , "tasks": tasks_per_node,
    "Output": Output
}

# ---------------------------------------------------------------------------------------------------------
# Dictionary to str ---------------------------------------------------------------------------------------
dump = json.dumps(input_para)

# Create Output directory ---------------------------------------------------------------------------------
line = " mkdir -p "+Output
os.system(line)

# Compute the map -----------------------------------------------------------------------------------------
os.system("OMP_NUM_THREADS=1 python multiprocessing_si.py '{:s}'".format(dump) )

print "All done for Node {:d}".format(node)
print " "
