"""
Benitez-Llambay, P. & Krapp L. 2019

General linear solution for the x-z linearized equations for a gas + N-dust species. 

"""

from pylab import *

def equilibrium_solution(eps, ts, q, xi, ok):

    """This function computes the equilibrium solutions for gas + len(ts)
    dust species. These solutions are needed for obtaining the
    eigenvalues of the linear problem

    Input: 
    
    esp: Array --> dust-to-gas mass ratio
    ts : Array --> Stokes numbers

    q  : Scalar --> Shear parameter (q=3/2 for a Keplerian box) 
    xi : Scalar --> magnitude of the velocity perturbation (usually,
         xi=-2*h**2*vk)
    
    Output: 

    vgx, vgy, vjx, vjy --> the equilibrium solutions.  vgx, vgy are
    scalar, and vjx, vjy are arrays of size len(ts), corresponding to
    the equilibrium solution for each fluid

    """
    
    kappa = sqrt(2*(2-q))
    
    A = (ok*eps/ts*(kappa**2*ts**2)/(1+kappa**2*ts**2)).sum()
    B = 1 + (ok*eps/(1+kappa**2*ts**2)).sum()

    psi = 1.0/(A**2+kappa**2*B**2)
    
    vgx =  A*psi*xi
    vgy =  -0.5*kappa**2*B*psi*xi     #The extra term accounting for the shear
                                      #is not relevant for the linear
                                      #streaming-instability analysis

    vjx = (vgx + 2*vgy*ts)/(1+kappa**2*ts**2)
    vjy = (vgy - (2-q)*vgx*ts)/(1+kappa**2*ts**2)
    
   
    return vgx, vgy, vjx, vjy

def compute_eigenvalues(eps,ts,Kx,Kz,q,h,vgx,vgy,vjx,vjy,vec):

    """Returns the eigenvalues and eigenvectors for the x-z linearized
    equations for a gas + N-dust species.

    Input:

    Kx, Kz: Scalar --> Wavenumbers. These values must be
                       dimensionless, defined as K =
                       k*(h**2*vk0/omega0)

    q : Scalar --> Shear parameter (q=3/2 for a Keplerian box) 
    h : Scalar --> h = cs/vk0 is the aspect-ratio of the disk.  

    vgx, vgy, vjx, vjy are the normalized equilibrium velocities 
                       obtained with equilibrium_solution(). 
                       They need to be normalized by h**2*vk.

    Output: w,v: normalized eigenvalues and normalized eigenvectors 
    of the problem. The eigenvalues are normalized by Omega_0, 
    and the eigenvectors associated to the eigenvalue w[i] is v[:,i]

    """
    
    n_dust    = eps.shape[0]
    n_species = eps.shape[0]+1
    
    #Two intermediate quantities
    i      = 1j           #imaginary number
    deltax = vgx-vjx
    deltay = vgy-vjy

    # Building the matrix -----------------------------------------------------
    matrix = zeros([4*n_species,4*n_species],dtype='complex128')
    
    #The shape of the matrix is: drhog, dvgx, dvgy, dvgz, drhoj, dvjx, dvjy, dvjz, ...

    #We first deal with the gas component
    #Continuity----------------------------------------------------------------
    matrix[0,0] = i*Kx*vgx #drhog
    matrix[0,1] = i*Kx     #dvgx
    matrix[0,3] = i*Kz     #dvgz

    #Momentum_x----------------------------------------------------------------
    matrix[1,0] =  i*Kx*h**(-2) - (eps*deltax/ts).sum()      #drhog
    matrix[1,1] =  i*Kx*vgx + (eps/ts).sum()     #dvgx
    matrix[1,2] = -2.0                           #dvgy
    for k in range(n_dust):
        matrix[1,4*(k+1)+0] =  deltax[k]/ts[k]   #drhoj
        matrix[1,4*(k+1)+1] = -eps[k]/ts[k]      #dvkx

    #Momentum_y----------------------------------------------------------------
    matrix[2,0] = -(eps*deltay/ts).sum()         #drhog
    matrix[2,1] =  2-q                           #dvgx
    matrix[2,2] =  i*Kx*vgx + (eps/ts).sum()     #dvgy
    for k in range(n_dust):
        matrix[2,4*(k+1)+0] =  deltay[k]/ts[k]   #drhok
        matrix[2,4*(k+1)+2] = -eps[k]/ts[k]      #dvky
        
    #Momentum_z----------------------------------------------------------------
    matrix[3,0] = i*Kz*h**(-2)                   #drhog
    matrix[3,3] = i*Kx*vgx + (eps/ts).sum()      #dvgz
    for k in range(n_dust):
        matrix[3,4*(k+1)+3] = -eps[k]/ts[k]      #dvkz

    #--------------------------------------------------------------------------
    #Now the dust component----------------------------------------------------    
    for k in range(n_dust):
        #Continuity
        matrix[4*(k+1),4*(k+1)+0] = i*Kx*vjx[k]             #drhok
        matrix[4*(k+1),4*(k+1)+1] = i*Kx*eps[k]             #dvkx
        matrix[4*(k+1),4*(k+1)+3] = i*Kz*eps[k]             #dvkz        
        #Momentum_x
        matrix[4*(k+1)+1,1]         = -1./ts[k]             #dvgx
        matrix[4*(k+1)+1,4*(k+1)+1] =  i*Kx*vjx[k]+1./ts[k] #dvkx
        matrix[4*(k+1)+1,4*(k+1)+2] = -2.                   #dvky
        #Momentum_y
        matrix[4*(k+1)+2,2]         = -1./ts[k]             #dvgy
        matrix[4*(k+1)+2,4*(k+1)+1] = 2.-q                  #dvkx
        matrix[4*(k+1)+2,4*(k+1)+2] = i*Kx*vjx[k]+1./ts[k]  #dvky
        #Momentum_z
        matrix[4*(k+1)+3,3]         = -1./ts[k]             #dvgz
        matrix[4*(k+1)+3,4*(k+1)+3] = i*Kx*vjx[k]+1./ts[k]  #dvkz
    #--------------------------------------------------------------------------


    v = 0
    if (vec==True):
        w, v = eig(matrix)
    else:
        w = eigvals(matrix)
        
    return w, v

