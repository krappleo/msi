### Multispecies (S)treaming (I)nstability #

This repository contains a series of python scripts that solve the eigenvalue problem for the multispecies streaming instability.
###### Developed by Pablo Benitez-Llambay and Leonardo Krapp.

------------------------

The core of the solver is inside: ```solver.py```.

The script has two functions:

```equilibrium_solution``` returns the steady-state background solution

```compute_eigenvalues```  returns the eigenvalues and eigenvctors. 

##### Example of use: 

Open ```example_run_no_parallel.py``` to set the input parameters.

Run the script in the terminal: ```python example_run_no_parallel.py```

------------------------

#### PARALLEL RUNS 
 
To obtain the solution for a large number of species, we recommend parallel computing.

We include two scripts that provide a suitable tool for parallel computing (it also can be used in 1 processor):

``` parallel_solver.py```

``` multiprocessing_si.py```

The script ```parallel_solver.py```  split the Kx-domain into a number of nodes. Each of these nodes will use multiprocessing to split the Kz-domain. 
In addition, the script allows the user to set up the parameters of the problem, a particle size distribution 
and the number of tasks for multiprocessing.

The script ```multiprocessing_si.py``` should not be modified, unless required by the user. It uses [multiprocessing](https://docs.python.org/2/library/multiprocessing.html) for parallel computing.

Both scripts are suitable for running in work stations or in a cluster.

##### Example of use: 

Assume that 2 nodes are available in a partition and each node has 10 processors.

a) Set the variable ```tasks_per_node = 10``` in parallel_solver.py

b) Run the script as follows (or equivalently send two jobs):
 
``` python parallel_solver.py 2 0 ```
 
``` python parallel_solver.py 2 1 ```
 
This will produce a map of the growth rate that is divided into two Outputs: maps/MAP_0.npy and maps/MAP_1.npy.

Besides, it returns the oscillatory frequency, maps/OSC_0.npy and maps/OSC_1.npy and the Kx,Kz domain of each node.

------------------------

#### Visualization of parallel runs:

Use the script ```build_map.py```. It requires the number of nodes and output directory as input. For instance, 
in the previous example run ```python build_map.py 2 maps```.