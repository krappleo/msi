"""
Benitez-Llambay, P. & Krapp L. 2019

Solution of the linear problem for the multispecies streaming instability. 
This script allows the user to visualize the growth rate as a function of KX and KZ.       
"""
from pylab import *
import sys

if len(sys.argv) == 1:
    print " "
    print "Run the script as:"    
    print "python build_map.py <total number of nodes> <output directory>"
    print " ============================================================ "
    print " "
    exit()
    
# Inputsfor the user -----------------------------------------------------------------------------------
ntot      = int(sys.argv[1])      #total number of nodes
directory = str(sys.argv[2])+"/"  #output directory

# Load data --------------------------------------------------------------------------------------------
kx        = np.load(directory+"kx_0.npy" )
kz        = np.load(directory+"kz_0.npy" ) #kz has no division 
maps      = np.load(directory+"MAP_0.npy")

for i in range(1,ntot):
    maps = np.append(maps, np.load(directory+"MAP_{:d}.npy".format(i)), axis=-1 )
    kx   = np.append( kx, np.load(directory+"kx_{:d}.npy".format(i)), axis=-1 )

MAP = maps

# Plot data --------------------------------------------------------------------------------------------
fig = plt.figure(figsize = (6,6))
MAP[MAP>=0] = nan
axes = subplot(1,1,1)
im1 = axes.contourf(kx,kz,log10(-MAP), origin='lower',extend='both',
                                 levels=linspace(-6,-0,25), cmap='RdBu_r')
axes.set_ylabel(r"$ K_z$")
axes.set_xlabel(r"$ K_x$")
axes.set_xscale('log')
axes.set_yscale('log')

cb = fig.colorbar(im1,ax=axes,extend='both', ticks=[-6,-5,-4,-3,-2,-1,0],
                      orientation="horizontal", pad=0.2, fraction=0.05)
cax = cb.ax
cax.vlines(0.5, 0., 1, colors = 'k', linewidth = 0.5, linestyles = 'dashed')
    
cb.set_label(r"$\log_{10} \sigma /\Omega_0$")
savefig("map.pdf")
show()


